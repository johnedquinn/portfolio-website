# Portfolio Website
Personal website containing information regarding my background, experience, and goals. The original website ran on a personal server, but it has since morphed into this project to further my web development skills.

## Project Goals
The goal of this site is to practice my web development skills (especially in regards to React) while showing my skills to recruiters.

## To Run
### Development
Run `npm run start` to run the app in development mode.<br/>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Deployment
Run `npm run build` to build the app for production to the `build` folder.<br/>
Then, run `serve -s build -l 80` to run the minified, optimized build on Port 80 for public access.

## View the Product
To view the website go to [http://www.johnedquinn.com](http://www.johnedquinn.com).