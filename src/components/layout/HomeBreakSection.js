/// @file:      HomeBreakSection.js
/// @author:    John Ed Quinn
// @desc:       Section on Home Page to ask user if they want to see projects or courses.

/* IMPORTS */
import React, { Fragment } from 'react';
import Button from 'react-bootstrap/Button';
import Github from '../icons/Github.svg';
import Gitlab from '../icons/GitLab.png';
import Resume from '../icons/Resume.png';

/* NAVBAR */
const HomeBreakSection = () => {
	return (
		<Fragment>
			<div className="header break-section">
				<div className="overlay"></div>
				<div className="container">
					<div className="row py-5"></div>
					<div className="col">
						<h1 className="text-white text-center">Take a Glimpse at my Work</h1>
						<p className="text-white text-center">My past work ranges from developing APIs and dynamic websites to building programming languages and web servers.</p>
						<div className="row">
							<div className="col col-3 text-center"></div>
							<div className="col col-2 text-center"><a href="https://github.com/johnedquinn" rel="noopener noreferrer" target="_blank"><img className="home-icon" alt="GitHub Icon" src={Github}/></a></div>
							<div className="col col-2 text-center"><a href="https://gitlab.com/johnedquinn" rel="noopener noreferrer" target="_blank"><img className="home-icon" alt="GitLab Icon" src={Gitlab}/></a></div>
							<div className="col col-2 text-center"><a href="https://drive.google.com/file/d/1rvOeW0j6wid5laUgBwpAPukAx5KJy5hN/view?usp=sharing" rel="noopener noreferrer" target="_blank"><img className="home-icon" alt="Resume Icon" src={Resume}/></a></div>
							<div className="col col-3 text-center"></div>
						</div>
					</div>
					<div className="row py-5"></div>
				</div>
			</div>

		</Fragment>
	);
}

/* EXPORTS */
export default HomeBreakSection;
